#!/usr/bin/env perl
#===============================================================================
#
#         FILE: oopsie.pl
#
#        USAGE: ./oopsie.pl
#
#  DESCRIPTION: Watches a directory recursively for changes and runs all tests
#               in it.
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2012-12-29 19:07
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use TAP::Harness;
use Linux::Inotify2;
use Data::Dumper;
use File::Spec::Functions qw(catfile);
use Cwd qw/cwd abs_path/;
use Getopt::Std;

$SIG{INT}  = \&terminate;
$SIG{TERM} = \&terminate;

my $ROOT;
my $LIB;
my $HARNESS;
my $WATCH = qr/\.(pl|pm|cgi|t)$/;
my $IGNORE;

sub init {
    if ($ARGV[0] !~ /^-/) {
        $ROOT = abs_path($ARGV[0]);
    }

    $ROOT ||= cwd;

    $LIB = "$ROOT/lib";

    unless (-d $ROOT) {
        print_help();
        exit 0;
    }

    $LIB = '' unless -d $LIB;

    $HARNESS = TAP::Harness->new(
        {
            lib       => ["$ROOT/t/lib", $LIB],
            color     => 1,
            verbosity => -2
        }
    );

    my $opts = {};
    getopt('w:i:h', $opts);

    if (exists $opts->{h}) {
        print_help();
        exit 0;
    }

    eval {
        if ($opts->{w}) {
            $WATCH = qr/$opts->{w}/;
        }

        if ($opts->{i}) {
            $IGNORE = qr/$opts->{i}/;
        }
    };

    if ($@) {
        chomp $@;
        print "Bad regex: $@\n";
        print_help();
        exit 0;
    }
}

main: {
    init();

    my $inotify = new Linux::Inotify2 or die "unable to create new inotify object: $!";
    watch_dir($inotify, $_) for get_directories();

    run_tests();

    1 while $inotify->poll;
}

sub get_directories {
    my $dir = shift || $ROOT;
    return map { chomp; $_ } `find $dir -type d -not -iwholename '*.git*'`;
}

sub get_test_files {
    my $dir = shift || $ROOT;
    my @test_files = map { chomp; $_ } `find $dir -name *.t`;

    if ($IGNORE) {
        @test_files = grep { !/$IGNORE/ } @test_files;
    }

    return @test_files;
}

sub watch_dir {
    my $inotify = shift;
    my $dir     = shift;

    $inotify->watch(
        $dir,
        IN_CREATE | IN_MODIFY,
        sub {
            my $e = shift;

            if ($e->IN_CREATE) {
                if (-d $e->fullname) {
                    watch_dir($inotify, $e->fullname);
                }
            } elsif ($e->IN_MODIFY) {
                if ($e->fullname =~ /$WATCH/) {
                    run_tests();
                }
            }
        }
    );
}

sub run_tests {
    system('clear');
    print_time();
    my @tests = get_test_files();
    my $result;

    eval { $result = $HARNESS->runtests(@tests); };

    if (!$@ and $result->all_passed) {
        `tmux set-option -g status-bg colour22`;
    } else {
        `tmux set-option -g status-bg colour52`;
    }
}

sub print_time {
    print '=' x 80, "\n";
    my $time = localtime;
    printf '%*s', (length($time) + 80) / 2, $time;
    print "\n", '=' x 80, "\n\n";
}

sub terminate {
    `tmux set-option -g status-bg colour234`;
    exit;
}

sub print_help {
    print <<HELP
oopsie [directory] -i regex -w regex

    directory - the directory that will be watched
    -i - ignore matching test files
    -w - watch matching files
    -h - help a.k.a this :)

HELP
        ;
}

